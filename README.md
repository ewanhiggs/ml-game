# ML Game 

## Introduction

After implementing the csv-game, I was interested to see which languages might
afford the best libraries and tools for perforaming data analysis on a medium
scale. 

## Procedure

This experiment uses a Naive Bayes Classifier to implement a sentiment analyser.
I've taken some [movie review
data](http://www.cs.cornell.edu/people/pabo/movie-review-data/) and tokenized it
and lemmatized it using Python's nltk. I then dumped it to json as a list of
token strings and a sentiment.

The features are merely the count of each token in the sample text.

Run `time movie-review ./data/movie-review/*.json`

## Timings

Here are some timings from my machine.
| Language                 | Time     | 
|--------------------------|---------:|
|C++ (rapidjson, stdlib)   | 0m0.000s |
|Python 2.7 (scikit-learn) | 0m0.000s |
