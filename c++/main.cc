#include "rapidjson/document.h"
#include "rapidjson/filereadstream.h"
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/date_time/posix_time/posix_time_io.hpp>
#include <unistd.h>
#include <iostream>
#include <cassert>
#include <fstream>
#include <vector>
#include <set>
#include <memory>
#include <string>
#include <random>
#include <algorithm>
#include <iterator>
#include <cmath>

typedef std::vector<std::string> tokens_t;
typedef std::vector<tokens_t> corpus_t;
typedef std::vector<int> sentiments_t;
typedef std::vector<double> feature_t;
typedef std::vector<feature_t> features_t;

struct klass_count {
    int klass;
    int count;
};

bool operator<(klass_count const& lhs, klass_count const& rhs) {
    return lhs.klass < rhs.klass;
}

typedef std::vector<klass_count> klass_counts_t;

struct prior {
    int klass;
    double probability;
};

bool operator<(prior const& lhs, prior const& rhs) {
    return lhs.klass < rhs.klass;
}

typedef std::vector<prior> priors_t;

static void set_time_format() {
    using namespace boost::posix_time;
    time_facet *facet = new time_facet("%Y/%m/%d %H:%M:%s");
    std::cout.imbue(std::locale(std::cout.getloc(), facet));
}

template<typename T>
void LOG(T t) { 
    using namespace boost::posix_time;
    std::cout << microsec_clock::local_time() << " " << t << "\n";
}

static void json_array_to_vector(rapidjson::Value& val, tokens_t& vec) {
    using namespace rapidjson;
    vec.reserve(val.Size());
    for (SizeType i = 0; i < val.Size(); i++){
        vec.push_back(val[i].GetString());
    }
}

static void load_json(int argc, char* argv[], corpus_t& corpus, sentiments_t& sentiments) {
    corpus.reserve(argc-1);
    sentiments.reserve(argc-1);

    using namespace rapidjson;
    char readBuffer[65536];

    for(int arg = 1; arg < argc; ++arg) {
        Document d;
        FILE* fp = fopen(argv[arg], "rb");
        FileReadStream is(fp, readBuffer, sizeof(readBuffer));
        d.ParseStream(is);
        fclose(fp);
        tokens_t tokens;
        json_array_to_vector(d["text"], tokens);
        corpus.push_back(tokens);
        sentiments.push_back(d["sentiment"].GetInt());
    }
}

static tokens_t find_vocabulary(corpus_t const& corpus) {
    tokens_t vocabulary;
    std::set<std::string> token_set;
    for(auto tokens : corpus) {
        for(auto token : tokens) {
            token_set.insert(token);
        }
    }
    vocabulary.reserve(token_set.size());
    std::copy(token_set.begin(), token_set.end(), std::back_inserter(vocabulary));
    return vocabulary;
}

static klass_counts_t find_klass_counts(sentiments_t const& sentiments) { 
    klass_counts_t counts;
    for (auto sentiment : sentiments) {
        auto state = std::find_if(std::begin(counts), std::end(counts), 
                [=] (decltype(*std::begin(counts)) s) { 
                return s.klass == sentiment;
                });
        if (state == counts.end()) {
            counts.push_back(klass_count{sentiment, 1});
        } else{
            state->count += 1;
        }
    }
    std::sort(counts.begin(), counts.end());
    return counts;
}

static priors_t find_klass_priors(klass_counts_t const& sentiment_counts, size_t nsentiments) { 
    priors_t klass_probs(sentiment_counts.size());
    for (size_t k = 0; k < sentiment_counts.size(); ++k) {
        prior & klass_prob = klass_probs[k];
        klass_prob.klass = sentiment_counts[k].klass;
        klass_prob.probability = sentiment_counts[k].count / static_cast<double>(nsentiments);
    }
    return klass_probs;
}

static feature_t text2feature(tokens_t const& text, 
                              tokens_t const& vocabulary) {
    feature_t f(vocabulary.size(), 0);
    for(auto token : text) {
        auto offset = std::lower_bound(vocabulary.begin(), vocabulary.end(), token);
        if (offset != vocabulary.end()) {
            f[offset - vocabulary.begin()] += 1.;
        }
    }
    return f;
}

static features_t corpus2features(corpus_t const& corpus,
                                  tokens_t const& vocabulary) {
    features_t features;
    features.reserve(corpus.size());
    for (auto text : corpus) {
        feature_t f = text2feature(text, vocabulary);
        features.push_back(f);
    }
    return features;
}

struct classifier_t {
private:
    priors_t priors;
    features_t features;

public:

    void train(features_t const& training_features, sentiments_t const& sentiments) {
        klass_counts_t scount = find_klass_counts(sentiments);
        priors = find_klass_priors(scount, sentiments.size());
        features.reserve(priors.size());
        for (size_t i = 0; i < priors.size(); ++i) { 
            features.push_back(feature_t(training_features[0].size(), 0));
        }
        for (size_t tf = 0; tf < training_features.size(); ++tf) { 
            auto p_idx = std::find_if(
                    priors.begin(), priors.end(), 
                    [=](decltype(*std::begin(priors)) k){
                        return k.klass == sentiments[tf];
                    });

            feature_t& klass_features = features[p_idx - priors.begin()];

            auto training_features_elem = training_features[tf];
            for (size_t f = 0; f < training_features_elem.size(); ++f) { 
                if ( training_features_elem[f] ) { 
                    klass_features[f] += 1;
                }
            }
        }
        for (size_t k = 0; k < features.size(); ++k) {
            feature_t& feat = features[k];
            for (size_t f = 0; f < features[k].size(); ++f) { 
                feat[f] /= scount[k].count;
            }
        }
    }

    int predict(feature_t const& feat) {
        auto jll = joint_log_likelihood(feat);
        auto jll_iter = std::max_element(jll.begin(), jll.end());
        return priors[jll_iter - jll.begin()].klass;
    }

private:
    feature_t joint_log_likelihood(feature_t const& feat) {
        feature_t jll(priors.size(), 0);
        for (size_t k = 0; k < priors.size(); ++ k) {
            double prob = log(priors[k].probability / (1. - priors[k].probability));
            feature_t const& probs = features[k];

            for (size_t f = 0; f < feat.size(); ++f) { 
                if (feat[f] && probs[f] && probs[f] != 1.0) {
                    prob += log(probs[f] / (1. - probs[f]));
                }
            }
            jll[k] = prob;
        }
        return jll;
    }
};


int main(int argc, char* argv[]) {
    if (argc < 2) {
        std::cerr << "Usage: movie-review <json-file1> [json-file2 ...]" 
                  << std::endl;
        exit(EXIT_FAILURE);
    }
    set_time_format();

    corpus_t total_corpus;
    sentiments_t total_sentiments;

    LOG("Loading JSON");
    load_json(argc, argv, total_corpus, total_sentiments);
    std::shuffle(total_corpus.begin(), total_corpus.end(), std::default_random_engine(1));
    std::shuffle(total_sentiments.begin(), total_sentiments.end(), std::default_random_engine(1));

    size_t split_idx = total_corpus.size() / 4;
    corpus_t training_corpus, test_corpus;
    corpus_t::const_iterator train_corp_begin = total_corpus.begin();
    corpus_t::const_iterator train_corp_split = total_corpus.begin() + split_idx;
    corpus_t::const_iterator test_corp_end   = total_corpus.end();
    std::copy(train_corp_begin, train_corp_split, std::back_inserter(training_corpus));
    std::copy(train_corp_split, test_corp_end, std::back_inserter(test_corpus));

    sentiments_t training_sentiments, test_sentiments;
    sentiments_t::const_iterator train_sent_begin = total_sentiments.begin();
    sentiments_t::const_iterator train_sent_split = total_sentiments.begin() + split_idx;
    sentiments_t::const_iterator test_sent_end   = total_sentiments.end();
    std::copy(train_sent_begin, train_sent_split, std::back_inserter(training_sentiments));
    std::copy(train_sent_split, test_sent_end, std::back_inserter(test_sentiments));

    LOG("Loaded JSON");
    LOG("Constructing Vocabulary");
    tokens_t vocabulary = find_vocabulary(training_corpus);
    LOG("Extracting Training Features");
    features_t training_features = corpus2features(training_corpus, vocabulary);
    classifier_t classifier;
    LOG("Training Begin");
    classifier.train(training_features, training_sentiments);
    LOG("Training End");

    LOG("Extracting Test Features");
    features_t test_features = corpus2features(test_corpus, vocabulary);
    LOG("Extracted Test Features");

    int correct = 0;
    int wrong = 0;
    for (size_t i = 0; i < test_features.size(); ++i) {
        auto f = test_features[i];
        auto s = test_sentiments[i];
        auto prediction = classifier.predict(f);
        //std::cout << prediction << " ";
        if (prediction == s) {
            ++correct;
        } else {
            ++wrong;
        }
    }
    std::cout << "correct: " << correct << " " << (double)correct / test_features.size() << "\n";
    std::cout << "wrong: " <<  wrong << " " << (double)wrong / test_features.size() << "\n";

    return EXIT_SUCCESS;
}
